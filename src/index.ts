import dotenv from "dotenv";
import express from "express";

import { createServer } from "http";
import { Server } from "socket.io";
import cors from 'cors';
import figlet from "figlet";

// initialize configuration
dotenv.config();

// port is now available to the Node.js runtime
// as if it were an environment variable
const port = process.env.SERVER_PORT;
const app = express();

app.use(cors());

const http = createServer(app);

const io = new Server(http, {
    cors: {
        origin: "*",
        methods: ['GET', 'POST']
    }
});


const seances: Record<string, string> = {};

io.on("connection", (socket) => {
    socket.on('auth', ({ seanceId }) => {
        if (!seances[seanceId]) {
            seances[seanceId] = socket.id;
        }
    });

    socket.on('ON_MESSAGE', ({ seanceId, message }) => {
        io.to(seances[seanceId]).emit("ON_MESSAGE", message);
    });

    socket.on("disconnect", () => {
        Object.entries(seances).map(([key, value]) => {
            if (value === socket.id) delete seances[key];
        });
    });
});

http.listen( port, () => {
    // tslint:disable-next-line:no-console
    figlet("Road Sockets", (err, data) => {
        if (err === null) {
            console.log("_____________");
            console.log("\x1b[32m%s\x1b[0m", data);
            console.log("\x1b[1m%s\x1b[0m", `\x1b[32m✔\x1b[0m Socket server listening at http://localhost:${ port }`);
        }
    });
} );
